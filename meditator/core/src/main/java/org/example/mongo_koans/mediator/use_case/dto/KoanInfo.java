package org.example.mongo_koans.mediator.use_case.dto;

import org.example.mongo_koans.mediator.model.koans.Koan;

public record KoanInfo(int order, String title, String description, String solution) {
    public KoanInfo(Koan koan) {
        this(koan.getOrder(), koan.getTitle(), koan.getDescription(), koan.getSolution());
    }
}

package org.example.mongo_koans.mediator.model.koans;

import java.util.function.BiPredicate;

public class Koan {
  private final int order;
  private final String title;
  private final String description;
  private final String before;
  private final String after;
  private final String expectedResult;
  private final VerificationType verificationType;
  private final String solution;

  public Koan(
      int order,
      String title,
      String description,
      String before,
      String after,
      String expectedResult,
      VerificationType verificationType,
      String solution) {
    this.order = order;
    this.title = title;
    this.description = description;
    this.before = before;
    this.after = after;
    this.expectedResult = expectedResult;
    this.verificationType = verificationType;
    this.solution = solution;
  }

  public int getOrder() {
    return order;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getBefore() {
    return before == null ? "" : before;
  }

  public String getAfter() {
    return after == null ? "" : after;
  }

  public String getExpectedResult() {
    return expectedResult;
  }

  public VerificationType getVerificationType() {
    return verificationType;
  }

  public String getCompleteStrCommands(String userCommand) {
    var userCommandToPut = userCommand.charAt(userCommand.length() - 1) == ';' ? userCommand : userCommand + ';';
    return before + userCommandToPut + getAfter();
  }

  public boolean isCorrect(String commandResult, BiPredicate<String, String> isProposeCorrect) {
    return isProposeCorrect.test(commandResult, expectedResult);
  }

  public String getSolution() {
    return solution;
  }
}

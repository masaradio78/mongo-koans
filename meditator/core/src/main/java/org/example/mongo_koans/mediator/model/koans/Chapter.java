package org.example.mongo_koans.mediator.model.koans;

import org.example.mongo_koans.mediator.use_case.exceptions.KoanNotFoundException;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Chapter {
  private final int number;
  private final String name;
  private final List<Koan> koans;

  public Chapter(int number, String name, List<Koan> koans) {
    this.number = number;
    this.name = name;
    this.koans = koans;
  }

  public int getNumber() {
    return number;
  }

  public String getName() {
    return name;
  }

  public List<Koan> getKoans() {
    return koans;
  }

  public Optional<Koan> findKoanByOrder(int order) {
    return koans.stream()
        .filter(koan -> koan.getOrder() == order)
        .findAny();
  }

  public Koan getKoanByOrder(int koanOrder) {
    return findKoanByOrder(koanOrder)
        .orElseThrow(() -> new KoanNotFoundException(
            MessageFormat.format("koan order {0} not found in chapter {1}", koanOrder, number))
        );
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Chapter chapter = (Chapter) o;
    return number == chapter.number;
  }

  @Override
  public int hashCode() {
    return Objects.hash(number);
  }
}

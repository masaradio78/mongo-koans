package org.example.mongo_koans.mediator.model.mongo_commands;

import java.util.List;

public class MongoCommands {
    private final List<MongoCommand> mongoCommandList;

    public MongoCommands(List<String> mongoCommandList) {
        if (mongoCommandList == null || mongoCommandList.isEmpty()) throw new IllegalArgumentException("data can't be null");
        this.mongoCommandList = mongoCommandList.stream().map(MongoCommand::new).toList();
    }

    public String toShellEvalOptions() {
        return this.mongoCommandList.stream()
                .map(MongoCommand::toShellEvalOption)
                .reduce("", (total, current) -> total + current);
    }
}

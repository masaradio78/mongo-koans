package org.example.mongo_koans.mediator.use_case.repositories;

import org.example.mongo_koans.mediator.model.koans.Chapter;

import java.util.List;
import java.util.Optional;

public interface ChapterRepository {
    void addChapter(Chapter chapter);

    List<Chapter> findAll();

    Optional<Chapter> findByNumber(int number);
}

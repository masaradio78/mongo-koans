package org.example.mongo_koans.mediator.use_case.commands;

import org.example.mongo_koans.mediator.model.result.CommandResult;

public interface ExecuteMongoCommands {
  CommandResult execute(String mongoCommandsStr, String containerName);
}

package org.example.mongo_koans.mediator.model.result;

public final class ErrorCommand extends CommandResult {
  private final int exitCode;

  public ErrorCommand(String message, int exitCode) {
    super(message);
    this.exitCode = exitCode;
  }

  public int getExitCode() {
    return exitCode;
  }
}

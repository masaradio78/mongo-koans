package org.example.mongo_koans.mediator.use_case.dto;

import org.example.mongo_koans.mediator.model.koans.Koan;

public record SummaryKoan(int order, String title) {
    public SummaryKoan(Koan koan) {
        this(koan.getOrder(), koan.getTitle());
    }
}

package org.example.mongo_koans.mediator.model.koans;

public enum VerificationType {
  WITHOUT_OBJECT_ID,
  CONTAINS_RESULT
}

package org.example.mongo_koans.mediator.use_case;

import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.result.CommandResult;
import org.example.mongo_koans.mediator.model.result.SuccessCommand;
import org.example.mongo_koans.mediator.use_case.commands.ExecuteMongoCommands;
import org.example.mongo_koans.mediator.use_case.dto.ResultAnswer;
import org.example.mongo_koans.mediator.use_case.exceptions.ChapterNotFoundException;
import org.example.mongo_koans.mediator.use_case.exceptions.KoanNotFoundException;
import org.example.mongo_koans.mediator.use_case.repositories.ChapterRepository;
import org.example.mongo_koans.mediator.use_case.verification.VerificationFactory;

import java.text.MessageFormat;

public class ProposeAnswer {
  private final ChapterRepository chapterRepository;
  private final VerificationFactory verificationFactory;
  private final ExecuteMongoCommands executeMongoCommands;

  public ProposeAnswer(ChapterRepository chapterRepository, VerificationFactory verificationFactory, ExecuteMongoCommands executeMongoCommands) {
    this.chapterRepository = chapterRepository;
    this.verificationFactory = verificationFactory;
    this.executeMongoCommands = executeMongoCommands;
  }

  public ResultAnswer execute(String answer, int chapterNumber, int koanOrder, String containerName) throws ChapterNotFoundException, KoanNotFoundException {
    var chapter = chapterRepository.findByNumber(chapterNumber)
        .orElseThrow(() -> new ChapterNotFoundException(MessageFormat.format("chapter {0} not found", chapterNumber)));
    var koan = chapter.getKoanByOrder(koanOrder);

    var commandResult = executeMongoCommands.execute(koan.getCompleteStrCommands(answer), containerName);

    return new ResultAnswer(commandResult, isProposeCorrect(koan, commandResult));
  }

  private boolean isProposeCorrect(Koan koan, CommandResult commandResult) {
    try {
      return commandResult instanceof SuccessCommand && koan.isCorrect(
          commandResult.getMessage(),
          verificationFactory.build(koan.getVerificationType())::isOk
      );
    } catch (Exception exception) {
      return false;
    }
  }
}

package org.example.mongo_koans.mediator.use_case;

import org.example.mongo_koans.mediator.use_case.dto.KoanInfo;
import org.example.mongo_koans.mediator.use_case.exceptions.ChapterNotFoundException;
import org.example.mongo_koans.mediator.use_case.exceptions.KoanNotFoundException;
import org.example.mongo_koans.mediator.use_case.repositories.ChapterRepository;

import java.text.MessageFormat;

public class GetOneKoan {
    private final ChapterRepository chapterRepository;

    public GetOneKoan(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }

    public KoanInfo execute(int chapterNumber, int koanOrder) throws ChapterNotFoundException, KoanNotFoundException {
        return chapterRepository.findByNumber(chapterNumber)
                .orElseThrow(() -> new ChapterNotFoundException(
                        MessageFormat.format("chapter {0} not found", chapterNumber))
                )
                .findKoanByOrder(koanOrder)
                .map(KoanInfo::new)
                .orElseThrow(() -> new KoanNotFoundException(
                                MessageFormat.format("koan order {0} not found in chapter {1}",
                                        koanOrder,
                                        chapterNumber)
                        )
                );
    }
}

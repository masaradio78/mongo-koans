package org.example.mongo_koans.mediator.model.mongo_commands;

import java.text.MessageFormat;

public class MongoCommand {
    private final String command;

    public MongoCommand(String command) {
        if (command == null || command.isBlank())
            throw new IllegalArgumentException("command can't be null neither empty");
        this.command = command.trim();
    }

    public String toShellEvalOption() {
        return MessageFormat.format(" --eval \"{0}\"", command);
    }
}

package org.example.mongo_koans.mediator.use_case.dto;

import org.example.mongo_koans.mediator.model.result.CommandResult;

public class ResultAnswer {
  private final CommandResult commandResult;
  private final boolean isProposeCorrect;

  public ResultAnswer(CommandResult commandResult, boolean isProposeCorrect) {
    this.commandResult = commandResult;
    this.isProposeCorrect = isProposeCorrect;
  }

  public CommandResult getCommandResult() {
    return commandResult;
  }

  public boolean isProposeCorrect() {
    return isProposeCorrect;
  }
}

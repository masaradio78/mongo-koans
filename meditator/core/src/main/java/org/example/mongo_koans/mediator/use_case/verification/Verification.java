package org.example.mongo_koans.mediator.use_case.verification;

public interface Verification {
  boolean isOk(String result, String expectedResult);
}

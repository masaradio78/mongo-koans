package org.example.mongo_koans.mediator.model.result;

public final class SuccessCommand extends CommandResult {
  public SuccessCommand(String message) {
    super(message);
  }

}

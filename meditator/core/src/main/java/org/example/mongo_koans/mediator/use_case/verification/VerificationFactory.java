package org.example.mongo_koans.mediator.use_case.verification;

import org.example.mongo_koans.mediator.model.koans.VerificationType;

public interface VerificationFactory {
  Verification build(VerificationType verificationType);
}

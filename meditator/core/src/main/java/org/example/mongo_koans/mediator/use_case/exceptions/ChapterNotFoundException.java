package org.example.mongo_koans.mediator.use_case.exceptions;

public class ChapterNotFoundException extends RuntimeException {
    public ChapterNotFoundException(String message) {
        super(message);
    }
}

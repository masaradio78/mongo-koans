package org.example.mongo_koans.mediator.use_case.exceptions;

public class KoanNotFoundException extends RuntimeException {
    public KoanNotFoundException(String message) {
        super(message);
    }
}

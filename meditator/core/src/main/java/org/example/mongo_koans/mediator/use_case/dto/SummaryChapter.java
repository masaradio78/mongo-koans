package org.example.mongo_koans.mediator.use_case.dto;

import org.example.mongo_koans.mediator.model.koans.Chapter;

import java.util.List;
import java.util.Objects;

public class SummaryChapter {
    private final int number;
    private final String name;
    private final List<SummaryKoan> summaryKoans;

    public SummaryChapter(int number, String name, List<SummaryKoan> summaryKoans) {
        this.number = number;
        this.name = name;
        this.summaryKoans = summaryKoans;
    }

    public SummaryChapter(Chapter chapter) {
        this.number = chapter.getNumber();
        this.name = chapter.getName();
        this.summaryKoans = chapter.getKoans()
                .stream()
                .map(SummaryKoan::new).toList();
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public List<SummaryKoan> getSummaryKoans() {
        return summaryKoans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SummaryChapter that = (SummaryChapter) o;
        return number == that.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return "ChapterInfo{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", koanInfos=" + summaryKoans +
                '}';
    }
}

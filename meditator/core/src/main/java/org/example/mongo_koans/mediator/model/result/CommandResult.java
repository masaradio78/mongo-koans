package org.example.mongo_koans.mediator.model.result;

public abstract sealed class CommandResult permits ErrorCommand, SuccessCommand {
  private final String message;

  protected CommandResult(String message) {
    this.message = message;
  }


  public String getMessage() {
    return message;
  }
}

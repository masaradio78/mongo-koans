package org.example.mongo_koans.mediator.use_case;

import org.example.mongo_koans.mediator.use_case.dto.SummaryChapter;
import org.example.mongo_koans.mediator.use_case.repositories.ChapterRepository;

import java.util.List;

public class GetAllKoans {
    private final ChapterRepository chapterRepository;

    public GetAllKoans(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }

    public List<SummaryChapter> execute() {
        return chapterRepository.findAll().stream()
                .map(SummaryChapter::new)
                .toList();
    }
}

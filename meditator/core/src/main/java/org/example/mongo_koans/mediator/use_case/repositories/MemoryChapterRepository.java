package org.example.mongo_koans.mediator.use_case.repositories;

import org.example.mongo_koans.mediator.model.koans.Chapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MemoryChapterRepository implements ChapterRepository {
    private final List<Chapter> chapters = new ArrayList<>();
    @Override
    public void addChapter(Chapter chapter) {
        chapters.add(chapter);
    }

    @Override
    public List<Chapter> findAll() {
        return chapters;
    }

    @Override
    public Optional<Chapter> findByNumber(int number) {
        return chapters.stream().filter(chapter -> chapter.getNumber() == number).findAny();
    }
}

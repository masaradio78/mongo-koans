Feature: Propose an answer to one koan
  As a person who is meditating
  I want to propose an answer to one koan
  To know if my answer is correct

  Scenario: Propose correct answer with koan that has commands before
    Given chapter 1 with name "find" with few koans
      | order | title                         | description                                                | before                                                                                                 | after | expectedResult                                                                              | verificationType  | solution                      |
      | 1     | find all and show only quotes | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insert({quote: 'esprit libre; environnement libre', category: 'meditation'}); |       | [{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }] | WITHOUT_OBJECT_ID | db.koans.find({}, {quote: 1}) |
    And container name "my-container-name123"
    When a person propose answer "db.koans.find({}, {quote: 1})" for koan in chapter 1 and order 1
    And command result is
      | "[{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }]" |
    And the propose is ok
    Then the answer should indicate that the propose is correct should contain command output

  Scenario: Propose correct answer with koan that has commands after
    Given chapter 1 with name "find" with few koans
      | order | title      | description                                                                          | before          | after            | expectedResult                                                                              | verificationType  | solution                                                                               |
      | 1     | insert one | launch command to insert one koans contain quote 'esprit libre; environnement libre' | use MongoKoans; | db.koans.find(); | [{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }] | WITHOUT_OBJECT_ID | db.koans.insert({quote: 'esprit libre; environnement libre', category: 'meditation'}); |
    When a person propose answer "db.koans.insert({quote: 'esprit libre; environnement libre'})" for koan in chapter 1 and order 1
    And command result is
      | "[{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }]" |
    And the propose is ok
    Then the answer should indicate that the propose is correct should contain command output

  Scenario: Propose not correct answer with koan
    Given chapter 1 with name "find" with few koans
      | order | title      | description                                                                          | before          | after            | expectedResult                                                                              | verificationType  | solution                                                                               |
      | 1     | insert one | launch command to insert one koans contain quote 'esprit libre; environnement libre' | use MongoKoans; | db.koans.find(); | [{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }] | WITHOUT_OBJECT_ID | db.koans.insert({quote: 'esprit libre; environnement libre', category: 'meditation'}); |
    When a person propose answer "db.koans.insert({quote: 'esprit libre; environnements libres'})" for koan in chapter 1 and order 1
    And command result is
      | "[{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnements libres' }]" |
    And the propose is not ok
    Then the answer should indicate that the propose is not correct and should contain command output

  Scenario: Chapter not found
    Given chapter 1 with name "find" with few koans
      | order | title      | description                                                                          | before          | after            | expectedResult                                                                              | verificationType  | solution                                                                               |
      | 1     | insert one | launch command to insert one koans contain quote 'esprit libre; environnement libre' | use MongoKoans; | db.koans.find(); | [{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }] | WITHOUT_OBJECT_ID | db.koans.insert({quote: 'esprit libre; environnement libre', category: 'meditation'}); |
    When a person propose answer "db.koans.insert({quote: 'esprit libre; environnements libres'})" for koan in chapter 2 and order 1
    And the answer proposed is executed with none existed chapter
    Then it should indicate that chapter 2 doesn't exist

  Scenario: Koan order not found in chapter
    Given chapter 1 with name "find" with few koans
      | order | title      | description                                                                          | before          | after            | expectedResult                                                                              | verificationType  | solution                                                                               |
      | 1     | insert one | launch command to insert one koans contain quote 'esprit libre; environnement libre' | use MongoKoans; | db.koans.find(); | [{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }] | WITHOUT_OBJECT_ID | db.koans.insert({quote: 'esprit libre; environnement libre', category: 'meditation'}); |
    When a person propose answer "db.koans.insert({quote: 'esprit libre; environnements libres'})" for koan in chapter 1 and order 2
    And the answer proposed is executed with none existed koan
    Then it should indicate that koan in order 2 doesn't exist in chapter 1

  Scenario: When answer is not correct command
    Given chapter 1 with name "find" with few koans
      | order | title      | description                                                                          | before          | after            | expectedResult                                                                              | verificationType  | solution                                                                               |
      | 1     | insert one | launch command to insert one koans contain quote 'esprit libre; environnement libre' | use MongoKoans; | db.koans.find(); | [{ _id: ObjectId("64b1c6262f77435476605ec3"), quote: 'esprit libre; environnement libre' }] | WITHOUT_OBJECT_ID | db.koans.insert({quote: 'esprit libre; environnement libre', category: 'meditation'}); |
    When a person propose not correct command "db.koans.insert({quote: 'esprit libre; environnements libres')" for koan in chapter 1 and order 1
    Then then answer should indicate that the command result is error
Feature: Get koans
  As a person who want to meditate
  I want to get koans to select one

  Scenario: Get all koans
    Given chapter 1 with name "create" with few koans
      | order | title                          | description                                                | before | after    | expectedResult | verificationType | solution        |
      | 1     | Create database and collection | launch command to be able to see new database "MongoKoans" |        | show dbs |                | CONTAINS_RESULT  | use MongoKoans; |
    And chapter 2 with name "find" with few koans
      | order | title                         | description                                                | before                                                                                                    | after | expectedResult | verificationType  | solution                      |
      | 1     | find all in collection        | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find()               |
      | 2     | find all and show only quotes | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find({}, {quote: 1}) |
    When a person get all koans
    Then the person should get chapter 1 with name "create" with few koans
      | order | title                          |
      | 1     | Create database and collection |
    And the person should get chapter 2 with name "find" with few koans
      | order | title                         |
      | 1     | find all in collection        |
      | 2     | find all and show only quotes |

Feature: Get one koan
  As a person who want to meditate
  I want to get one koan

  Scenario: Get one existed koan
    Given chapter 1 with name "create" with few koans
      | order | title                          | description                                                | before | after    | expectedResult | verificationType | solution        |
      | 1     | Create database and collection | launch command to be able to see new database "MongoKoans" |        | show dbs |                | CONTAINS_RESULT  | use MongoKoans; |
    And chapter 2 with name "find" with few koans
      | order | title                         | description                                                | before                                                                                                    | after | expectedResult | verificationType  | solution                      |
      | 1     | find all in collection        | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find()               |
      | 2     | find all and show only quotes | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find({}, {quote: 1}) |
    When a person want koan contain in chapter 2 and in order 1
    Then the person should get a koan with
      | order | title                  | description                                                | solution        |
      | 1     | find all in collection | launch command to be able to see new database "MongoKoans" | db.koans.find() |

  Scenario: Exception when chapter number not exist
    Given chapter 1 with name "create" with few koans
      | order | title                          | description                                                | before | after    | expectedResult | verificationType | solution        |
      | 1     | Create database and collection | launch command to be able to see new database "MongoKoans" |        | show dbs |                | CONTAINS_RESULT  | use MongoKoans; |
    And chapter 2 with name "find" with few koans
      | order | title                         | description                                                | before                                                                                                    | after | expectedResult | verificationType  | solution                      |
      | 1     | find all in collection        | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find()               |
      | 2     | find all and show only quotes | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find({}, {quote: 1}) |
    When a person want not existed koan contain in chapter 3 and in order 1
    Then it should indicate that chapter 3 doesn't exist

  Scenario: Exception when koan order not exist in chapter
    Given chapter 1 with name "create" with few koans
      | order | title                          | description                                                | before | after    | expectedResult | verificationType | solution        |
      | 1     | Create database and collection | launch command to be able to see new database "MongoKoans" |        | show dbs |                | CONTAINS_RESULT  | use MongoKoans; |
    And chapter 2 with name "find" with few koans
      | order | title                         | description                                                | before                                                                                                    | after | expectedResult | verificationType  | solution                      |
      | 1     | find all in collection        | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find()               |
      | 2     | find all and show only quotes | launch command to be able to see new database "MongoKoans" | use MongoKoans; db.koans.insertMany({quote: 'esprit libre; environnement libre', category: 'meditation'}) |       |                | WITHOUT_OBJECT_ID | db.koans.find({}, {quote: 1}) |
    When a person want not existed koan contain in chapter 1 and in order 2
    Then it should indicate that koan in order 2 doesn't exist in chapter 1
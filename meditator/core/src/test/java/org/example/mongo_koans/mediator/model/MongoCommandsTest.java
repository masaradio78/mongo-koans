package org.example.mongo_koans.mediator.model;

import org.example.mongo_koans.mediator.model.mongo_commands.MongoCommands;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;

class MongoCommandsTest {
    @Test
    void when_one_command_show_dbs_should_return_one_eval_with_command_show_dbs() {
        var mongoCommands = new MongoCommands(List.of("show dbs"));

        assertThat(mongoCommands.toShellEvalOptions()).isEqualTo(" --eval \"show dbs\"");
    }

    @Test
    void when_two_commands_should_return_two_eval_with_given_commands() {
        var mongoCommands = new MongoCommands(List.of("use koans", "db.koans.find()"));

        assertThat(mongoCommands.toShellEvalOptions()).isEqualTo(" --eval \"use koans\" --eval \"db.koans.find()\"");
    }


    @Test
    void when_two_commands_with_line_break_should_return_two_eval_with_given_commands() {
        var mongoCommands = new MongoCommands(List.of("use koans" + System.lineSeparator(), "db.koans.find()\n"));
        assertThat(mongoCommands.toShellEvalOptions()).isEqualTo(" --eval \"use koans\" --eval \"db.koans.find()\"");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_data_is_null_should_throw_exception(List<String> nullOrEmptyData) {
        var throwable = catchThrowable(() -> new MongoCommands(nullOrEmptyData));

        assertThat(throwable).isExactlyInstanceOf(IllegalArgumentException.class);
        assertThat(throwable.getMessage()).isEqualTo("data can't be null");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_one_command_is_null_or_empty_should_throw_exception(String nullOrEmptyCommand) {
        var throwable = catchThrowable(() -> {
            var mongoCommandList = new ArrayList<>(Collections.singleton(nullOrEmptyCommand));
            new MongoCommands(mongoCommandList);
        });

        assertThat(throwable).isExactlyInstanceOf(IllegalArgumentException.class);
        assertThat(throwable.getMessage()).isEqualTo("command can't be null neither empty");
    }
}
package org.example.mongo_koans.mediator;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.koans.VerificationType;
import org.example.mongo_koans.mediator.model.result.ErrorCommand;
import org.example.mongo_koans.mediator.model.result.SuccessCommand;
import org.example.mongo_koans.mediator.use_case.GetAllKoans;
import org.example.mongo_koans.mediator.use_case.GetOneKoan;
import org.example.mongo_koans.mediator.use_case.ProposeAnswer;
import org.example.mongo_koans.mediator.use_case.commands.ExecuteMongoCommands;
import org.example.mongo_koans.mediator.use_case.dto.KoanInfo;
import org.example.mongo_koans.mediator.use_case.dto.ResultAnswer;
import org.example.mongo_koans.mediator.use_case.dto.SummaryChapter;
import org.example.mongo_koans.mediator.use_case.dto.SummaryKoan;
import org.example.mongo_koans.mediator.use_case.exceptions.ChapterNotFoundException;
import org.example.mongo_koans.mediator.use_case.exceptions.KoanNotFoundException;
import org.example.mongo_koans.mediator.use_case.repositories.ChapterRepository;
import org.example.mongo_koans.mediator.use_case.repositories.MemoryChapterRepository;
import org.example.mongo_koans.mediator.use_case.verification.Verification;
import org.example.mongo_koans.mediator.use_case.verification.VerificationFactory;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.assertj.core.api.AssertionsForClassTypes.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class KoansSteps {
  private ChapterRepository chapterRepository;
  private GetAllKoans getAllKoans;
  private GetOneKoan getOneKoan;
  private ProposeAnswer proposeAnswer;
  private List<SummaryChapter> summaryChapters;
  private KoanInfo koanInfo;
  private Throwable throwable;
  private ExecuteMongoCommands executeMongoCommands;
  private VerificationFactory verificationFactory;
  private String answer;
  private int chapterNumber;
  private int koanOrder;
  private String commandOutput;
  private ResultAnswer resultAnswer;
  private ErrorCommand commandResult;
  private Verification verification;
  private String containerName;

  @Before
  public void setUp() {
    chapterRepository = new MemoryChapterRepository();
    executeMongoCommands = mock(ExecuteMongoCommands.class);
    verificationFactory = mock(VerificationFactory.class);
    verification = mock(Verification.class);

    getAllKoans = new GetAllKoans(chapterRepository);
    getOneKoan = new GetOneKoan(chapterRepository);
    proposeAnswer = new ProposeAnswer(chapterRepository, verificationFactory, executeMongoCommands);
  }

  @Given("chapter {int} with name {string} with few koans")
  public void chapterWithNameWithFewKoans(int chapterNumber, String chapterName, DataTable dataTable) {
    var koans = getKoans(dataTable, map -> new Koan(
        Integer.parseInt(map.get("order")),
        map.get("title"),
        map.get("description"),
        map.get("before"),
        map.get("after"),
        map.get("expectedResult"),
        VerificationType.valueOf(map.get("verificationType")),
        map.get("solution"))
    );

    chapterRepository.addChapter(new Chapter(chapterNumber, chapterName, koans));
  }

  private static <T> List<T> getKoans(DataTable dataTable, Function<Map<String, String>, T> to) {
    return dataTable.asMaps(String.class, String.class).stream()
        .map(to)
        .toList();
  }

  @When("a person get all koans")
  public void aPersonGetAllKoans() {
    summaryChapters = getAllKoans.execute();
  }

  @When("a person want koan contain in chapter {int} and in order {int}")
  public void aPersonWantKoanContainInChapterAndInOrder(int chapterNumber, int koanOrder) throws ChapterNotFoundException, KoanNotFoundException {
    koanInfo = getOneKoan.execute(chapterNumber, koanOrder);
  }

  @Then("the person should get chapter {int} with name {string} with few koans")
  public void thePersonShouldGetChapterWithNameWithFewKoans(int order, String chapterName, DataTable dataTable) {
    var maybeChapter = summaryChapters.stream().filter(chapter -> chapter.getName().equals(chapterName)).findAny();

    var koans = getKoans(dataTable, map -> new SummaryKoan(
        Integer.parseInt(map.get("order")),
        map.get("title"))
    );

    var expectedChapter = new SummaryChapter(order, chapterName, koans);
    maybeChapter.ifPresentOrElse(chapterInfo -> assertThat(chapterInfo).usingRecursiveComparison()
            .isEqualTo(expectedChapter),
        () -> fail("chapter is null")
    );
  }

  @Then("the person should get a koan with")
  public void thePersonShouldGetAKoanWith(DataTable dataTable) {
    var koan = getKoans(dataTable, map -> new KoanInfo(
        Integer.parseInt(map.get("order")),
        map.get("title"),
        map.get("description"),
        map.get("solution")
    )).get(0);

    assertThat(koanInfo).isEqualTo(koan);
  }

  @When("a person want not existed koan contain in chapter {int} and in order {int}")
  public void aPersonWantNotExistedKoanContainInChapterAndInOrder(int chapterNumber, int koanOrder) {
    throwable = catchThrowable(() -> getOneKoan.execute(chapterNumber, koanOrder));
  }

  @Then("it should indicate that chapter {int} doesn't exist")
  public void itShouldIndicateThatChapterDoesnTExist(int chapterNumber) {
    assertThat(throwable).isExactlyInstanceOf(ChapterNotFoundException.class);
    assertThat(throwable.getMessage()).isEqualTo(MessageFormat.format("chapter {0} not found", chapterNumber));
  }

  @Then("it should indicate that koan in order {int} doesn't exist in chapter {int}")
  public void itShouldIndicateThatKoanInOrderDoesnTExistInChapter(int koanOrder, int chapterNumber) {
    assertThat(throwable).isExactlyInstanceOf(KoanNotFoundException.class);
    assertThat(throwable.getMessage()).isEqualTo(MessageFormat.format("koan order {0} not found in chapter {1}", koanOrder, chapterNumber));
  }

  @When("a person propose answer {string} for koan in chapter {int} and order {int}")
  public void aPersonProposeAnswerForKoanInChapterAndOrder(String answer, int chapterNumber, int koanOrder) {
    this.answer = answer;
    this.chapterNumber = chapterNumber;
    this.koanOrder = koanOrder;
  }

  @And("command result is")
  public void commandResultIsBCFEc(DataTable dataTable) {
    this.commandOutput = dataTable.asList().get(0);
  }

  @Then("the answer should indicate that the propose is correct should contain command output")
  public void theAnswerShouldIndicateThatTheProposeIsCorrectShouldContainCommandOutput() throws ChapterNotFoundException, KoanNotFoundException {
    var koan = chapterRepository.findByNumber(chapterNumber).flatMap(byNumber -> byNumber
        .findKoanByOrder(koanOrder)).orElseThrow();
    var completeStrCommands = getCompleteStrCommands(koan);

    when(executeMongoCommands.execute(completeStrCommands, containerName)).thenReturn(new SuccessCommand(commandOutput));

    var result = proposeAnswer.execute(answer, chapterNumber, koanOrder, containerName);

    assertThat(result).isNotNull();
    assertThat(result.getCommandResult()).isExactlyInstanceOf(SuccessCommand.class);
    assertThat(result.getCommandResult().getMessage()).isEqualTo(commandOutput);
    assertThat(result.isProposeCorrect()).isTrue();
  }

  @Then("the answer should indicate that the propose is not correct and should contain command output")
  public void theAnswerShouldIndicateThatTheProposeIsNotCorrectAndShouldContainCommandOutput() throws ChapterNotFoundException, KoanNotFoundException {
    var koan = chapterRepository.findByNumber(chapterNumber).orElseThrow()
        .findKoanByOrder(koanOrder).orElseThrow();
    String completeStrCommands = getCompleteStrCommands(koan);

    when(executeMongoCommands.execute(completeStrCommands, containerName)).thenReturn(new SuccessCommand(commandOutput));

    var result = proposeAnswer.execute(answer, chapterNumber, koanOrder, containerName);

    assertThat(result).isNotNull();
    assertThat(result.getCommandResult()).isExactlyInstanceOf(SuccessCommand.class);
    assertThat(result.getCommandResult().getMessage()).isEqualTo(commandOutput);
    assertThat(result.isProposeCorrect()).isFalse();
  }

  private String getCompleteStrCommands(Koan koan) {
    var answerToPut = answer.charAt(answer.length() - 1) == ';' ? answer : answer + ';';
    return koan.getBefore() + answerToPut + koan.getAfter();
  }

  @And("the answer proposed is executed with none existed chapter")
  public void theAnswerProposedIsExecutedWithNoneExistedChapter() {
    throwable = catchThrowable(() -> proposeAnswer.execute(answer, chapterNumber, koanOrder, containerName));
  }

  @And("the answer proposed is executed with none existed koan")
  public void theAnswerProposedIsExecutedWithNoneExistedKoan() {
    throwable = catchThrowable(() -> proposeAnswer.execute(answer, chapterNumber, koanOrder, containerName));
  }

  @And("container name {string}")
  public void containerName(String containerName) {
    this.containerName = containerName;
  }

  @When("a person propose not correct command {string} for koan in chapter {int} and order {int}")
  public void aPersonProposeNotCorrectCommandForKoanInChapterAndOrder(String notCorrectCommand, int chapterNumber, int koanOrder) throws ChapterNotFoundException, KoanNotFoundException {
    var koan = chapterRepository.findByNumber(chapterNumber).orElseThrow()
        .findKoanByOrder(koanOrder).orElseThrow();
    answer = notCorrectCommand;
    commandResult = new ErrorCommand("error in command", 1);
    when(executeMongoCommands.execute(getCompleteStrCommands(koan), containerName)).thenReturn(commandResult);
    resultAnswer = proposeAnswer.execute(notCorrectCommand, chapterNumber, koanOrder, containerName);
  }

  @Then("then answer should indicate that the command result is error")
  public void thenAnswerShouldIndicateThatTheCommandResultIsError() {
    assertThat(resultAnswer.getCommandResult()).isExactlyInstanceOf(ErrorCommand.class);
    assertThat(resultAnswer.getCommandResult()).isEqualTo(commandResult);
    assertThat(resultAnswer.isProposeCorrect()).isFalse();
  }

  @And("the propose is ok")
  public void theProposeIsOk() {
    var currentKoan = chapterRepository.findByNumber(chapterNumber).flatMap(byNumber -> byNumber
        .findKoanByOrder(koanOrder)).orElseThrow();
    when(verification.isOk(commandOutput, currentKoan.getExpectedResult())).thenReturn(true);

    when(verificationFactory.build(currentKoan.getVerificationType())).thenReturn(verification);
  }

  @And("the propose is not ok")
  public void theProposeIsNotOk() {
    var currentKoan = chapterRepository.findByNumber(chapterNumber).flatMap(byNumber -> byNumber
        .findKoanByOrder(koanOrder)).orElseThrow();
    when(verification.isOk(commandOutput, currentKoan.getExpectedResult())).thenReturn(false);

    when(verificationFactory.build(currentKoan.getVerificationType())).thenReturn(verification);
  }
}

package com.example.mongo_koans.mediator.infra.verification;

import com.example.mongo_koans.mediator.infra.parser.BsonParser;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class VerificationWithoutObjectIdTest {
  private final BsonParser bsonParser = new BsonParser();

  private final VerificationWithoutObjectId verificationWithoutObjectId = new VerificationWithoutObjectId(bsonParser);

  @Test
  void when_result_and_expected_result_are_document_and_only_object_id_are_different_should_return_true() {
    var bsonDocument = "{ _id: ObjectId(\"64b27c010cdd15d34f179721\"), toto: 'tata', titi: [{tutu: 'tyty'}] }";
    var bsonDocument2 = "{ _id: ObjectId(\"64b27c010cdd15d34f179722\"), toto: 'tata', titi: [{tutu: 'tyty'}] }";

    assertThat(verificationWithoutObjectId.isOk(bsonDocument, bsonDocument2)).isTrue();
  }

  @Test
  void when_result_and_expected_result_are_document_and_more_than_object_id_are_different_should_return_false() {
    var bsonDocument = "{ _id: ObjectId(\"64b27c010cdd15d34f179721\"), toto: 'tata', titi: [{tutu: 'tyti'}] }";
    var bsonDocument2 = "{ _id: ObjectId(\"64b27c010cdd15d34f179722\"), toto: 'tata', titi: [{tutu: 'tyty'}] }";

    assertThat(verificationWithoutObjectId.isOk(bsonDocument, bsonDocument2)).isFalse();
  }

  @Test
  void when_result_and_expected_result_are_string_array_of_document_and_only_object_id_are_different_should_return_true() {
    var arrayDocuments = """
        [
          { _id: ObjectId("64b1c68202553135ae678c85"), toto: 'tata' },
          { _id: ObjectId("64b1c68202553135ae678c86"), toto: ['titi'] }
        ]
        """;
    var expectedArrayDocuments = """
        [
          { _id: ObjectId("64b1c68202553135ae678c87"), toto: 'tata' },
          { _id: ObjectId("64b1c68202553135ae678c88"), toto: ['titi'] }
        ]
        """;

    assertThat(verificationWithoutObjectId.isOk(arrayDocuments, expectedArrayDocuments)).isTrue();
  }

  @Test
  void when_result_and_expected_result_are_string_array_of_document_and_are_different_should_return_false() {
    var arrayDocuments = """
        [
          { _id: ObjectId("64b1c68202553135ae678c85"), toto: 'tata' },
          { _id: ObjectId("64b1c68202553135ae678c86"), toto: ['tutu'] }
        ]
        """;
    var expectedArrayDocuments = """
        [
          { _id: ObjectId("64b1c68202553135ae678c85"), toto: 'tata' },
          { _id: ObjectId("64b1c68202553135ae678c86"), toto: ['titi'] }
        ]
        """;
    assertThat(verificationWithoutObjectId.isOk(arrayDocuments, expectedArrayDocuments)).isFalse();
  }

  @Disabled
  @Test
  void when_result_and_expected_result_are_date_of_document_and_are_different_should_return_false() {
    var documentWithDate = """
        [
        { _id: ObjectId("64b1c68202553135ae678c85"), theDate: new ISODate('2021-01-01') }
                ]
                """;
    var expectedDocumentWithDate = """
        [
        { _id: ObjectId("64b1c68202553135ae678c85") }
               ]
                """;
    assertThat(verificationWithoutObjectId.isOk(documentWithDate, expectedDocumentWithDate)).isFalse();
  }
}
package com.example.mongo_koans.mediator.infra.parser;

import org.bson.Document;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BsonParserTest {
  private final BsonParser bsonParser = new BsonParser();

  @Nested
  class ParseOneDocument {
    @Test
    void when_json_is_one_document_should_parse_to_one_document() {
      var jsonDocument = "{ _id: ObjectId(\"64b27c010cdd15d34f179722\"), toto: 'tata', titi: [{tutu: 'tyty'}] }";

      Document result = bsonParser.parseOneDocument(jsonDocument);

      Document expect = new Document();
      expect.put("_id", "64b27c010cdd15d34f179722");
      expect.put("toto", "tata");
      expect.put("titi", List.of(new Document("tutu", "tyty")));
      assertThat(result).isEqualTo(expect);
    }

    @Test
    void when_json_contain_ISODate_should_parse() {
      var jsonDocument = "{ _id: ObjectId(\"64b27c010cdd15d34f179722\"), theDate: new ISODate('2020-01-01') }";

      Document result = bsonParser.parseOneDocument(jsonDocument);

      Document expect = new Document();
      expect.put("_id", "64b27c010cdd15d34f179722");
      expect.put("theDate", Date.from(LocalDate.of(2020, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()));
      assertThat(result).isEqualTo(expect);
    }
  }

  @Nested
  class ParseDocuments {
    @Test
    void when_json_is_array_of_documents_should_parse_documents() {
      var result = bsonParser.parseDocuments("""
          [
            { _id: ObjectId("64b1c68202553135ae678c85"), toto: 'tata' },
            { _id: ObjectId("64b1c68202553135ae678c86"), toto: ['titi'] }
          ]
          """);

      var expected = List.of(
          new Document(Map.of("_id", "64b1c68202553135ae678c85", "toto", "tata")),
          new Document(Map.of("_id", "64b1c68202553135ae678c86", "toto", List.of("titi")))
      );
      assertThat(result).isEqualTo(expected);
    }

    @Disabled
    @Test
    void when_json_contain_ISODate_should_parse() {
      var jsonDocument = "[{ _id: ObjectId(\"64b27c010cdd15d34f179722\"), theDate: new ISODate('2020-01-01') }]";

      var result = bsonParser.parseDocuments(jsonDocument);

      Document expect = new Document();
      expect.put("_id", "64b27c010cdd15d34f179722");
      expect.put("theDate", Date.from(LocalDate.of(2020, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()));
      assertThat(result).isEqualTo(List.of(expect));
    }
  }
}
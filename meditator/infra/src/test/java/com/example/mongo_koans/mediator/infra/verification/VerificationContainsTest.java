package com.example.mongo_koans.mediator.infra.verification;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class VerificationContainsTest {
  private final VerificationContains verificationContains = new VerificationContains();

  @Test
  void when_result_contains_expectedResult_should_return_true() {
    assertThat(verificationContains.isOk("admin xxxMB\n local xxxMB\n koans xxxMB\n", "koans")).isTrue();
  }

  @Test
  void when_result_not_contains_expectedResult_should_return_false() {
    assertThat(verificationContains.isOk("admin xxxMB\n local xxxMB\n nop xxxMB\n", "koans")).isFalse();
  }
}
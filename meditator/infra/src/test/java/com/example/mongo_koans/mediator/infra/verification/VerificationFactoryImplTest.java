package com.example.mongo_koans.mediator.infra.verification;

import com.example.mongo_koans.mediator.infra.parser.BsonParser;
import org.example.mongo_koans.mediator.model.koans.VerificationType;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

class VerificationFactoryImplTest {
  private final BsonParser bsonParser = Mockito.mock(BsonParser.class);
  private final VerificationFactoryImpl verificationFactory = new VerificationFactoryImpl(bsonParser);

  @Test
  void when_verification_type_is_WITHOUT_OBJECT_ID_then_return_verification_without_object_id() {
    assertThat(verificationFactory.build(VerificationType.WITHOUT_OBJECT_ID)).isExactlyInstanceOf(VerificationWithoutObjectId.class);
  }

  @Test
  void when_verification_type_is_CONTAINS_RESULT_then_return_verification_contains() {
    assertThat(verificationFactory.build(VerificationType.CONTAINS_RESULT)).isExactlyInstanceOf(VerificationContains.class);
  }
}
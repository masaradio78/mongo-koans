package com.example.mongo_koans.mediator.infra.io;

import org.example.mongo_koans.mediator.model.result.ErrorCommand;
import org.example.mongo_koans.mediator.model.result.SuccessCommand;
import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DockerRunMongoCommandTest {
  @Mock
  private NameGenerator mockNameGenerator;
  @Mock
  private CommandExecutor mockCommandExecutor;

  private DockerRunMongoCommand dockerRunMongoCommand;

  @BeforeEach
  void setup() {
    dockerRunMongoCommand = new DockerRunMongoCommand(mockNameGenerator, mockCommandExecutor);
  }

  @Test
  void when_command_generate_error_should_throw_exception() {
    when(mockNameGenerator.generate()).thenReturn("container-name");
    when(mockCommandExecutor.execute("docker run -d --name container-name mongo"))
        .thenReturn(new ErrorCommand("error", 1));

    var throwable = catchThrowable(() -> dockerRunMongoCommand.execute());

    assertThat(throwable).isExactlyInstanceOf(DockerCommandErrorException.class);
  }

  @Test
  void when_command_execute_without_error_should_return_docker_result() {
    var containerName = "container-name";
    when(mockNameGenerator.generate()).thenReturn(containerName);
    when(mockCommandExecutor.execute("docker run -d --name container-name mongo"))
        .thenReturn(new SuccessCommand("success"));

    var result = dockerRunMongoCommand.execute();

    var expected = DockerResultAnswer.builder()
        .containerName(containerName)
        .build();
    assertThat(result).usingRecursiveComparison().isEqualTo(expected);
  }
}
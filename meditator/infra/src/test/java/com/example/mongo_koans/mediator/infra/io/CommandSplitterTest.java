package com.example.mongo_koans.mediator.infra.io;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CommandSplitterTest {
  @Test
  void given_simple_command_should_return_splitted_command() {
    assertThat(CommandSplitter.split("docker run mongo"))
        .isEqualTo(new String[] {"docker", "run", "mongo"});
  }

  @Test
  void when_command_with_quote_should_return_splitted_command_without_quote_one() {
    assertThat(CommandSplitter.split("docker start mongo --eval 'use koans'"))
        .isEqualTo(new String[] {"docker", "start", "mongo", "--eval", "'use koans'"});
  }
}
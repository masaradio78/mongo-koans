package com.example.mongo_koans.mediator.infra.io;

import com.example.mongo_koans.mediator.infra.helpers.DateHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NameGeneratorTest {
  @Mock
  DateHelper mockDateHelper;
  private NameGenerator nameGenerator;

  @BeforeEach
  void setup() {
    nameGenerator = new NameGenerator(mockDateHelper);
  }

  @Test
  void without_prefix_and_postfix_should_name_will_be_time() {
    var today = LocalDateTime.parse("2023-07-27T19:09:10.628046833");
    when(mockDateHelper.now()).thenReturn(today);

    assertThat(nameGenerator.generate()).isEqualTo("2023-07-27T19-09-10-628046833");
  }

  @Test
  void with_prefix_name_should_name_will_contain_prefix_and_portion_of_time_max_30_length() {
    var today = LocalDateTime.parse("2023-07-27T19:09:10.628046833");
    when(mockDateHelper.now()).thenReturn(today);

    assertThat(nameGenerator.generate("mongo")).isEqualTo("mongo2023-07-27T19-09-10-62804");
  }
}
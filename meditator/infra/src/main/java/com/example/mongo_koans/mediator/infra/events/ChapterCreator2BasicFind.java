package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.koans.VerificationType;

import java.util.List;

public class ChapterCreator2BasicFind implements ChapterCreator {
  private Koan createKoan1() {
    var descriptionChapter2Koan1 = """
        Propose command to find all koans in collection **koans**. [About find command](https://www.mongodb.com/docs/manual/reference/command/find/)
        #### Command before your answer :\s
        ```
        use mongo_koans;
        db.koans.insertMany([{koan: 'first'}, {koan: 'second'}]);
        ```
        #### Expected result :\s
        ```
        [
          {_id: ObjectId("xxxxxxxxxxxxxxxx"), koan: 'first'},
          {_id: ObjectId("yyyyyyyyyyyyyyyy"), koan: 'second'}
        ]
        ```
                """;
    return new Koan(
        1,
        "Find all element in koans",
        descriptionChapter2Koan1,
        "use mongo_koans; db.koans.insertMany([{koan: 'first'}, {koan: 'second'}]);",
        "",
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), koan: 'first'}, {_id: ObjectId(\"64b32033eb5c5e320771e625\"), koan: 'second'}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.find();"
    );
  }

  private Koan createKoan2() {

    return new Koan(
        2,
        "Find with a simple filter",
        """
            Find list of koan with only first one.
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([{koan: 'first'}, {koan: 'second'}]);
            ```
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), koan: 'first'}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{koan: 'first'}, {koan: 'second'}]);",
        "",
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), koan: 'first'}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.find({koan: 'first'});"
    );
  }

  private Koan createKoan3Projection() {
    return new Koan(
        3,
        "Find with a simple projection",
        """
            Find all koans with only object id and name. [About Projection](https://www.mongodb.com/docs/v5.0/tutorial/project-fields-from-query-results/)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2}
            ]);
            ```
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'first'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second'}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2}]);",
        "",
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'first'}, {_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second'}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.find({}, {name: 1});"
    );
  }

  private Koan createKoan4Projection() {
    return new Koan(
        4,
        "Find with a projection without id",
        """
            Find all koans with only name without id.
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2}
            ]);
            ```
            #### Expected result :\s
            ```
            [
              {name: 'first'},
              {name: 'second'}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2}]);",
        "",
        "[{name: 'first'}, {name: 'second'}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.find({}, {name: 1, _id: 0});"
    );
  }

  private Koan createKoan5FindOne() {
    return new Koan(
        5,
        "Find one",
        """
             Find the second koan.
             #### Command before your answer :\s
             ```
             use mongo_koans;
             db.koans.insertMany([
               {name: 'first', order: 1},
               {name: 'second', order: 2},
               {name: 'third', order: 3}
             ]);
             ```
             #### Expected result :\s
             ```
            {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', order: 2}
             ```
                     """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2}, {name: 'third', order: 3}]);",
        "",
        "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', order: 2}",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.findOne({order: 2});"
    );
  }

  private Koan createKoan6SortSkipLimit() {
    return new Koan(
        6,
        "Find precised koans",
        """
            Find the second and third koans by order. You need to use sort, skip and limit.
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2},
              {name: 'third', order: 3},
              {name: 'fourth', order: 4},
              {name: 'fifth', order: 5},
              {name: 'sixth', order: 6}
            ]);
            ```
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', order: 2},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'third', order: 3}
            ]
                       
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([" +
            "{name: 'first', order: 1}," +
            "{name: 'fourth', order: 4}," +
            "{name: 'third', order: 3}," +
            "{name: 'fifth', order: 5}," +
            "{name: 'second', order: 2}," +
            "{name: 'sixth', order: 6}]);",
        "",
        "[" +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', order: 2}," +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'third', order: 3}" +
            "]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.find({}).sort({order: 1}).skip(1).limit(2);"
    );
  }

  @Override
  public Chapter create() {
    return new Chapter(2, "Find", List.of(
        createKoan1(),
        createKoan2(),
        createKoan3Projection(),
        createKoan4Projection(),
        createKoan5FindOne(),
        createKoan6SortSkipLimit()
    ));
  }
}

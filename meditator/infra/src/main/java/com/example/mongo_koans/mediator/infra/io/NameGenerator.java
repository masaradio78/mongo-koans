package com.example.mongo_koans.mediator.infra.io;

import com.example.mongo_koans.mediator.infra.helpers.DateHelper;
import org.springframework.stereotype.Component;

@Component
public class NameGenerator {
  private final DateHelper dateHelper;

  public NameGenerator(DateHelper dateHelper) {
    this.dateHelper = dateHelper;
  }

  public String generate() {
    return dateHelper.now().toString()
        .replaceAll("[:.]", "-");
  }

  public String generate(String prefix) {
    var result = prefix + generate();
    return result.length() < 30 ? result : result.substring(0, 30);
  }
}

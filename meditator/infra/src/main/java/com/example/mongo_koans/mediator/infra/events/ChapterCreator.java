package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;

public interface ChapterCreator {
  Chapter create();
}

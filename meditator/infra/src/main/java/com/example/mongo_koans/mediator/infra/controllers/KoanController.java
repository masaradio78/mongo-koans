package com.example.mongo_koans.mediator.infra.controllers;

import com.example.mongo_koans.mediator.infra.io.DockerExecMongoCommand;
import com.example.mongo_koans.mediator.infra.io.DockerResultAnswer;
import com.example.mongo_koans.mediator.infra.io.DockerRunMongoCommand;
import com.example.mongo_koans.mediator.infra.io.DockerStopMongoCommand;
import com.example.mongo_koans.mediator.infra.io.RemoveMongoContainerCommand;
import org.example.mongo_koans.mediator.model.result.CommandResult;
import org.example.mongo_koans.mediator.use_case.GetAllKoans;
import org.example.mongo_koans.mediator.use_case.GetOneKoan;
import org.example.mongo_koans.mediator.use_case.ProposeAnswer;
import org.example.mongo_koans.mediator.use_case.dto.KoanInfo;
import org.example.mongo_koans.mediator.use_case.dto.ResultAnswer;
import org.example.mongo_koans.mediator.use_case.dto.SummaryChapter;
import org.example.mongo_koans.mediator.use_case.exceptions.ChapterNotFoundException;
import org.example.mongo_koans.mediator.use_case.exceptions.KoanNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("chapter")
public class KoanController {
  private final Logger log = LoggerFactory.getLogger(this.getClass());
  private final ExecutorService executorService = Executors.newCachedThreadPool();
  private final DockerRunMongoCommand dockerRunMongoCommand;
  private final DockerExecMongoCommand dockerExecMongoCommand;
  private final DockerStopMongoCommand dockerStopMongoCommand;
  private final RemoveMongoContainerCommand removeMongoContainerCommand;
  private final GetAllKoans getAllKoans;
  private final GetOneKoan getOneKoan;
  private final ProposeAnswer proposeAnswer;

  public KoanController(DockerRunMongoCommand dockerRunMongoCommand, DockerExecMongoCommand dockerExecMongoCommand, DockerStopMongoCommand dockerStopMongoCommand, RemoveMongoContainerCommand removeMongoContainerCommand, GetAllKoans getAllKoans, GetOneKoan getOneKoan, ProposeAnswer proposeAnswer) {
    this.dockerRunMongoCommand = dockerRunMongoCommand;
    this.dockerExecMongoCommand = dockerExecMongoCommand;
    this.dockerStopMongoCommand = dockerStopMongoCommand;
    this.removeMongoContainerCommand = removeMongoContainerCommand;
    this.getAllKoans = getAllKoans;
    this.getOneKoan = getOneKoan;
    this.proposeAnswer = proposeAnswer;
  }

  @PostMapping("/test")
  public CompletableFuture<ResponseEntity<CommandResult>> runCommand(@RequestBody RunCommandRequest request) {
    return CompletableFuture
        .supplyAsync(dockerRunMongoCommand::execute, executorService)
        .thenApplyAsync(result -> {
          var commandResult = dockerExecMongoCommand.execute(request.getCommands(), result.getContainerName());
          return DockerResultAnswer.builder()
              .commandResult(commandResult)
              .isProposeCorrect(false)
              .containerName(result.getContainerName())
              .build();
        })
        .thenApplyAsync(output -> {
          dockerStopMongoCommand.execute(output.getContainerName());
          return output;
        })
        .thenApplyAsync(output -> {
          removeMongoContainerCommand.execute(output.getContainerName());
          return output;
        })
        .thenApplyAsync((result -> {
          var commandResult = result.getCommandResult();
          log.info(commandResult.getMessage());
          return ResponseEntity.ok(commandResult);
        }));
  }

  @GetMapping
  public ResponseEntity<List<SummaryChapter>> getAllKoansChapters() {
    return ResponseEntity.ok(getAllKoans.execute());
  }

  @GetMapping("/{number}/koan/{order}")
  public ResponseEntity<KoanInfo> getOneKoan(@PathVariable int number, @PathVariable int order) throws ChapterNotFoundException, KoanNotFoundException {
    return ResponseEntity.ok(getOneKoan.execute(number, order));
  }

  @PostMapping("/{number}/koan/{order}")
  public CompletableFuture<ResponseEntity<ResultAnswer>> runCommand(
      @PathVariable int number,
      @PathVariable int order,
      @RequestBody RunCommandRequest request
  ) {
    return CompletableFuture
        .supplyAsync(dockerRunMongoCommand::execute, executorService)
        .thenApplyAsync(result -> {
          var resultAnswer = proposeAnswer.execute(request.getCommands(), number, order, result.getContainerName());
          return DockerResultAnswer.builder(resultAnswer).containerName(result.getContainerName()).build();
        })
        .thenApplyAsync(result -> {
          dockerStopMongoCommand.execute(result.getContainerName());
          return result;
        })
        .thenApplyAsync(result -> {
          removeMongoContainerCommand.execute(result.getContainerName());
          return result;
        })
        .thenApplyAsync(answer -> {
          log.info(answer.getCommandResult().getMessage());
          return ResponseEntity.ok(answer);
        });
  }
}

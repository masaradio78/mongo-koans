package com.example.mongo_koans.mediator.infra.io;


import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoveMongoContainerCommand {
  private final CommandExecutor commandExecutor;
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public RemoveMongoContainerCommand(CommandExecutor commandExecutor) {
    this.commandExecutor = commandExecutor;
  }

  public void execute(String containerName) {
    var commandRmContainerResult = commandExecutor.execute("docker rm " + containerName);
    log.info("docker container {} removed", containerName);
    log.info("rm command message : {}", commandRmContainerResult.getMessage());
    var commandVolumePruneResult = commandExecutor.execute("docker volume prune -f");
    log.info("docker prune, result message {}", commandVolumePruneResult.getMessage());
  }
}

package com.example.mongo_koans.mediator.infra.io;

import org.example.mongo_koans.mediator.model.result.CommandResult;
import org.example.mongo_koans.mediator.use_case.dto.ResultAnswer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class DockerResultAnswer extends ResultAnswer {
  private final String containerName;

  private DockerResultAnswer(@NotNull ResultAnswer resultAnswer, String containerName) {
    super(resultAnswer.getCommandResult(), resultAnswer.isProposeCorrect());
    this.containerName = containerName;
  }

  private DockerResultAnswer(String containerName) {
    super(null, false);
    this.containerName = containerName;
  }

  public String getContainerName() {
    return containerName;
  }

  @Contract(value = " -> new", pure = true)
  public static @NotNull DockerResultAnswerBuilder builder() {
    return new DockerResultAnswerBuilder();
  }

  public static @NotNull DockerResultAnswerBuilder builder(ResultAnswer resultAnswer) {
    return new DockerResultAnswerBuilder(resultAnswer);
  }

  public static final class DockerResultAnswerBuilder {
    private CommandResult commandResult;
    private boolean isProposeCorrect;
    private String containerName;

    private DockerResultAnswerBuilder() {
    }

    public DockerResultAnswerBuilder(ResultAnswer resultAnswer) {
      commandResult = resultAnswer.getCommandResult();
      isProposeCorrect = resultAnswer.isProposeCorrect();
    }

    public DockerResultAnswerBuilder commandResult(CommandResult commandResult) {
      this.commandResult = commandResult;
      return this;
    }

    public DockerResultAnswerBuilder isProposeCorrect(boolean isProposeCorrect) {
      this.isProposeCorrect = isProposeCorrect;
      return this;
    }

    public DockerResultAnswerBuilder containerName(String containerName) {
      this.containerName = containerName;
      return this;
    }

    public DockerResultAnswer build() {
      if (this.commandResult == null) {
        return new DockerResultAnswer(containerName);
      }
      return new DockerResultAnswer(new ResultAnswer(this.commandResult, this.isProposeCorrect), containerName);
    }
  }

  @Override
  public String toString() {
    return "DockerResultAnswer{" +
        "containerName='" + containerName + '\'' +
        '}';
  }
}

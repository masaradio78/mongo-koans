package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.use_case.repositories.ChapterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Stream;

@Component
public class SeedKoansEvents implements ApplicationListener<ContextRefreshedEvent> {
  Logger log = LoggerFactory.getLogger(this.getClass());
  private final ChapterRepository chapterRepository;

  public SeedKoansEvents(ChapterRepository chapterRepository) {
    this.chapterRepository = chapterRepository;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    List<Chapter> chapters = Stream.of(
            new ChapterCreator1DatabaseAndCollection(),
            new ChapterCreator2BasicFind(),
            new ChapterCreator3BasicInsert(),
            new ChapterCreator4Delete(),
            new ChapterCreator5Update()
        )
        .map(ChapterCreator::create)
        .toList();
    for (Chapter chapter : chapters) {
      chapterRepository.addChapter(chapter);
    }

    log.info("seek koans done");
  }
}

package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.koans.VerificationType;

import java.util.List;

public class ChapterCreator4Delete implements ChapterCreator {
  private static final String DB_KOANS_FIND = "db.koans.find();";

  private Koan createKoan1() {
    return new Koan(
        1,
        "Delete one koan",
        """
            Delete first koan. [About deleteOne](https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteOne/)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2}
            ]);
            ```
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', order: 2}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2}]);",
        DB_KOANS_FIND,
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', order: 2}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteOne({name: 'first'});"
    );
  }

  private Koan createKoan2() {
    return new Koan(
        2,
        "Delete many koans",
        """
            Delete **Wu men guan** koans. [About deleteMany](https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteMany/)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', source: 'Wu men guan'},
              {name: 'second', source: 'Denkoroku'},
              {name: 'third', source: 'Wu men guan' }
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', source: 'Denkoroku'}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', source: 'Wu men guan'}, {name: 'second', source: 'Denkoroku'},{name: 'third', source: 'Wu men guan'}]);",
        DB_KOANS_FIND,
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', source: 'Denkoroku'}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteMany({source: 'Wu men guan'});"
    );
  }

  private Koan createKoan3() {
    return new Koan(
        3,
        "Delete koans order 2 and more",
        """
            Delete koans order greater or equal than 2. [About query operators](https://www.mongodb.com/docs/manual/reference/operator/query/#std-label-query-selectors)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2},
              {name: 'third', order: 3}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'first', order: 1}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2},{name: 'third', order: 3}]);",
        DB_KOANS_FIND,
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'first', order: 1}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteMany({order: {$gte: 2});"
    );
  }

  private Koan createKoan4() {
    return new Koan(
        4,
        "Delete one koan by id",
        """
            Delete last koan by id
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {_id: 'firstId', order: 1},
              {_id: 'secondId', order: 2},
              {_id: 'thirdId', order: 3}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: 'firstId', order: 1},
              {_id: 'secondId', order: 2}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{_id: 'firstId', order: 1}, {_id: 'secondId', order: 2}, {_id: 'thirdId', order: 3}]);",
        DB_KOANS_FIND,
        "[{_id: 'firstId', order: 1},{_id: 'secondId', order: 2}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteOne({_id: 'thirdId');"
    );
  }

  private Koan createKoan5() {
    return new Koan(
        5,
        "Delete all koans",
        """
            Delete all koans
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {_id: 'firstId', order: 1},
              {_id: 'secondId', order: 2},
              {_id: 'thirdId', order: 3}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            []
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{_id: 'firstId', order: 1}, {_id: 'secondId', order: 2}, {_id: 'thirdId', order: 3}]);",
        DB_KOANS_FIND,
        "[]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteMany({});"
    );
  }

  private Koan createKoan6() {
    return new Koan(
        6,
        "Delete all koans with regex",
        """
            Delete all koans start id with **s**. [About regex operator](https://www.mongodb.com/docs/manual/reference/operator/query/regex/#mongodb-query-op.-regex)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {_id: 'firstId', order: 1},
              {_id: 'secondId', order: 2},
              {_id: 'thirdId', order: 3},
              {_id: 'fourthId', order: 4},
              {_id: 'fifthId', order: 5},
              {_id: 'sixthId', order: 6}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: 'firstId', order: 1},
              {_id: 'thirdId', order: 3},
              {_id: 'fourthId', order: 4},
              {_id: 'fifthId', order: 5}
            ]
            ```
                    """,
        """
            use mongo_koans; db.koans.insertMany([
            {_id: 'firstId', order: 1},
            {_id: 'secondId', order: 2},
            {_id: 'thirdId', order: 3},
            {_id: 'fourthId', order: 4},
            {_id: 'fifthId', order: 5},
            {_id: 'sixthId', order: 6}]);
            """,
        DB_KOANS_FIND,
        "[{_id: 'firstId', order: 1},{_id: 'thirdId', order: 3}, {_id: 'fourthId', order: 4}, {_id: 'fifthId', order: 5}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteMany({_id: { $regex: /^s/ }});"
    );
  }

  private Koan createKoan7() {
    return new Koan(
        7,
        "Delete many koans with one or another values",
        """
            Delete **Wu men guan** or **Denkoroku** koans. [About $in operator](https://www.mongodb.com/docs/manual/reference/operator/query/in/#mongodb-query-op.-in)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', source: 'Wu men guan'},
              {name: 'second', source: 'Denkoroku'},
              {name: 'third', source: 'Wu men guan' },
              {name: 'fourth', source: 'Unknown' },
              {name: 'fifth', source: 'Wu men guan' },
              {name: 'sixth', source: 'Unknown' },
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'fourth', source: 'Unknown'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'sixth', source: 'Unknown'}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([" +
            "{name: 'first', source: 'Wu men guan'}, " +
            "{name: 'second', source: 'Denkoroku'}," +
            "{name: 'third', source: 'Wu men guan'}," +
            "{name: 'fourth', source: 'Unknown'}," +
            "{name: 'fifth', source: 'Wu men guan'}," +
            "{name: 'sixth', source: 'Unknown'}" +
            "]);",
        DB_KOANS_FIND,
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'fourth', source: 'Unknown'}, {_id: ObjectId(\"64b32033eb5c5e320771e625\"), name: 'sixth', source: 'Unknown'}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteMany({source: {$in: ['Wu men guan', 'Denkoroku']});"
    );
  }

  private Koan createKoan8() {
    return new Koan(
        8,
        "Delete many koans contain specific property",
        """
            Delete koans contain order property. [About $exists operator](https://www.mongodb.com/docs/manual/reference/operator/query/exists/#mongodb-query-op.-exists)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', source: 'Wu men guan', order: 1},
              {name: 'second', source: 'Denkoroku'},
              {name: 'third', source: 'Wu men guan' order: 3},
              {name: 'fourth', source: 'Unknown' },
              {name: 'fifth', source: 'Wu men guan' },
              {name: 'sixth', source: 'Unknown' },
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', source: 'Denkoroku'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'fourth', source: 'Unknown'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'fifth', source: 'Wu men guan'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'sixth', source: 'Unknown'},
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([" +
            "{name: 'first', source: 'Wu men guan', order: 1}," +
            "{name: 'second', source: 'Denkoroku'}," +
            "{name: 'third', source: 'Wu men guan', order: 3}," +
            "{name: 'fourth', source: 'Unknown'}," +
            "{name: 'fifth', source: 'Wu men guan'}," +
            "{name: 'sixth', source: 'Unknown'}" +
            "]);",
        DB_KOANS_FIND,
        "[" +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', source: 'Denkoroku'}," +
            "{_id: ObjectId(\"64b32033eb5c5e320771e625\"), name: 'fourth', source: 'Unknown'}," +
            "{_id: ObjectId(\"64b32033eb5c5e320771e626\"), name: 'fifth', source: 'Wu men guan'}," +
            "{_id: ObjectId(\"64b32033eb5c5e320771e627\"), name: 'sixth', source: 'Unknown'}" +
            "]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.deleteMany({source: {$exists: true]});"
    );
  }

  @Override
  public Chapter create() {
    return new Chapter(4, "Delete", List.of(
        createKoan1(),
        createKoan2(),
        createKoan3(),
        createKoan4(),
        createKoan5(),
        createKoan6(),
        createKoan7(),
        createKoan8()
    ));
  }
}

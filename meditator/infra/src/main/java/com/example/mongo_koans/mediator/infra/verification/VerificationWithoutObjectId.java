package com.example.mongo_koans.mediator.infra.verification;

import com.example.mongo_koans.mediator.infra.parser.BsonParser;
import org.example.mongo_koans.mediator.use_case.verification.Verification;

public class VerificationWithoutObjectId implements Verification {
  private final BsonParser bsonParser;

  public VerificationWithoutObjectId(BsonParser bsonParser) {
    this.bsonParser = bsonParser;
  }

  @Override
  public boolean isOk(String result, String expectedResult) {
    var resultWithSameObjectIds = replaceToSameObjectId(result);
    var expectedResultWithSameObjectIds = replaceToSameObjectId(expectedResult);

    if (expectedResult.trim().charAt(0) == '[') {
      return bsonParser.parseDocuments(resultWithSameObjectIds).equals(bsonParser.parseDocuments(expectedResultWithSameObjectIds));
    }
    return bsonParser.parseOneDocument(resultWithSameObjectIds).equals(bsonParser.parseOneDocument(expectedResultWithSameObjectIds));
  }

  private static String replaceToSameObjectId(String result) {
    return result.replaceAll("ObjectId\\(\"(.*?)\"\\)", "\"64b27c010cdd15d34f99999\"");
  }
}

package com.example.mongo_koans.mediator.infra.io;

import org.example.mongo_koans.mediator.model.result.SuccessCommand;
import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DockerRunMongoCommand {
  private static final String DOCKER_RUN_MONGO_CONTAINER_FORMAT = "docker run -d --name %s mongo";
  private final NameGenerator nameGenerator;
  private final CommandExecutor commandExecutor;
  private final Logger log = LoggerFactory.getLogger(this.getClass());


  public DockerRunMongoCommand(NameGenerator nameGenerator, CommandExecutor commandExecutor) {
    this.nameGenerator = nameGenerator;
    this.commandExecutor = commandExecutor;
  }

  public DockerResultAnswer execute() throws DockerCommandErrorException {
    var containerName = nameGenerator.generate();
    log.info("container name {}", containerName);
    var result = commandExecutor.execute(String.format(DOCKER_RUN_MONGO_CONTAINER_FORMAT, containerName));
    if (Objects.requireNonNull(result) instanceof SuccessCommand) {
      return DockerResultAnswer.builder().containerName(containerName).build();
    }
    throw new DockerCommandErrorException("Problem when run mongo container with container name " + containerName);
  }
}

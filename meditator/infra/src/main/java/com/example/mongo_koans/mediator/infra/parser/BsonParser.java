package com.example.mongo_koans.mediator.infra.parser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bson.BsonInvalidOperationException;
import org.bson.Document;
import org.bson.json.JsonParseException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BsonParser {
  public Document parseOneDocument(String input) throws JsonParseException {
    // Remove new lines and extra spaces
    String cleanedString = input.replaceAll("\\s+", " ");
    cleanedString = cleanedString.replaceAll("ObjectId\\(\"(.*?)\"\\)", "\"$1\"");

    // Parse the JSON array into a list of Documents
    try {
      return Document.parse(cleanedString);
    } catch (BsonInvalidOperationException exception) {
      throw new JsonParseException("Problem : " + exception.getMessage());
    }
  }

  public List<Document> parseDocuments(String jsonArray) {
    List<Document> documentArray = new ArrayList<>();
    jsonArray = jsonArray.replaceAll("\\s+", "");
    jsonArray = jsonArray.replaceAll("(\\w+):", "\"$1\":");
    jsonArray = jsonArray.replaceAll("ObjectId\\(\"(.*?)\"\\)", "\"$1\""); // Replace ObjectId references with their values
    jsonArray = jsonArray.replace("'", "\""); // Replace single quotes with double quotes

    try {
      List<String> jsonDocuments = transformStringToArray(jsonArray); // Split the JSON array into individual documents
      for (String jsonDocument : jsonDocuments) {
        Document document = Document.parse(jsonDocument);
        documentArray.add(document);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return documentArray;
  }

  private static List<String> transformStringToArray(String jsonString) {
    List<String> objectArray = new ArrayList<>();
    try {
      ObjectMapper mapper = new ObjectMapper();
      JsonNode jsonNode = mapper.readTree(jsonString);
      if (jsonNode.isArray()) {
        for (JsonNode element : jsonNode) {
          objectArray.add(element.toString());
        }
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    return objectArray;
  }
}

package com.example.mongo_koans.mediator.infra.io;

import org.example.mongo_koans.mediator.model.mongo_commands.MongoCommands;
import org.example.mongo_koans.mediator.model.result.CommandResult;
import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.example.mongo_koans.mediator.use_case.commands.ExecuteMongoCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DockerExecMongoCommand implements ExecuteMongoCommands {
  private final Logger log = LoggerFactory.getLogger(this.getClass());
  private final CommandExecutor commandExecutor;
  private static final String DOCKER_EXEC_MONGOSH = "docker exec %s /bin/mongosh --quiet ";

  public DockerExecMongoCommand(CommandExecutor commandExecutor) {
    this.commandExecutor = commandExecutor;
  }

  public CommandResult execute(String mongoCommandsStr, String containerName) {
    log.info("execute commands '{}'", mongoCommandsStr);
    String mongoCommands = new MongoCommands(Arrays.asList(mongoCommandsStr.split(";"))).toShellEvalOptions();
    String command = String.format(DOCKER_EXEC_MONGOSH, containerName) + mongoCommands;
    var result = commandExecutor.execute(command);
    int count = 0;
    while (result.getMessage().contains("connect ECONNREFUSED 127.0.0.1:27017") && count < 5) {
      log.warn("result connection problem : {}", result.getMessage());
      result = commandExecutor.execute(command);
      count++;
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    return result;
  }
}

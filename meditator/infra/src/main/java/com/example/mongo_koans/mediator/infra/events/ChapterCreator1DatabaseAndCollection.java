package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.koans.VerificationType;

import java.util.List;

public class ChapterCreator1DatabaseAndCollection implements ChapterCreator {
  private Koan createKoan1() {
    var descriptionChapter1Koan1 = """
        Launch enough commands to create new database **mongo_koans** and to be displayed in list of databases.
        After your commands when we launch  `show dbs` we should see the database **mongo_koans**.

        [How to create database with cli](https://www.mongodb.com/basics/create-database#creating-a-mongodb-database-with-the-cli)

        #### Command after your answer :
        `show dbs;`
        #### Expected result :\s
        ```\s
        admin ...
        ....
        mongo_koans XXXGB
                """;
    return new Koan(
        1,
        "Create database",
        descriptionChapter1Koan1,
        "",
        "show dbs;",
        "\nmongo_koans ",
        VerificationType.CONTAINS_RESULT,
        "use mongo_koans; db.koans.insertMany([{koan: 'test'}]);"
    );
  }
  private Koan createKoan2() {
    var descriptionChapter1Koan2 = """
        Create collection **koans** in already used database.  [About collection](https://www.mongodb.com/docs/manual/reference/method/db.createCollection/)
        #### Command before your answer : `use mongo_koans;` \s
        #### Command after your answer : `db.getCollectionNames();` \s
        #### Expected result : `[ 'koans' ]`
                """;
    return new Koan(
        2,
        "Create collection",
        descriptionChapter1Koan2,
        "use mongo_koans;",
        "db.getCollectionNames();",
        "[ 'koans' ]",
        VerificationType.CONTAINS_RESULT,
        "db.createCollection('koans');"
    );
  }
  @Override
  public Chapter create() {
    return new Chapter(
        1,
        "Create database and collection",
        List.of(
            createKoan1(),
            createKoan2()
        )
    );
  }
}

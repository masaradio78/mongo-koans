package com.example.mongo_koans.mediator.infra.controllers;

public class RunCommandRequest {
    private String commands;

    public String getCommands() {
        return commands;
    }

    public void setCommands(String commands) {
        this.commands = commands;
    }
}

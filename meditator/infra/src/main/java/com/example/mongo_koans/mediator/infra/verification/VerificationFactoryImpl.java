package com.example.mongo_koans.mediator.infra.verification;

import com.example.mongo_koans.mediator.infra.parser.BsonParser;
import org.example.mongo_koans.mediator.model.koans.VerificationType;
import org.example.mongo_koans.mediator.use_case.verification.Verification;
import org.example.mongo_koans.mediator.use_case.verification.VerificationFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class VerificationFactoryImpl implements VerificationFactory {
  private final BsonParser bsonParser;

  public VerificationFactoryImpl(BsonParser bsonParser) {
    this.bsonParser = bsonParser;
  }

  @Override
  public Verification build(VerificationType verificationType) {
    if (Objects.requireNonNull(verificationType) == VerificationType.WITHOUT_OBJECT_ID) {
      return new VerificationWithoutObjectId(bsonParser);
    } else if (verificationType == VerificationType.CONTAINS_RESULT) {
      return new VerificationContains();
    }
    throw new IllegalArgumentException();
  }
}

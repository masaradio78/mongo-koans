package com.example.mongo_koans.mediator.infra.io;

public class ProcessRuntimeException extends RuntimeException {
    public ProcessRuntimeException(Throwable cause) {
        super(cause);
    }
}

package com.example.mongo_koans.mediator.infra.verification;

import org.example.mongo_koans.mediator.use_case.verification.Verification;

public class VerificationContains implements Verification {
  @Override
  public boolean isOk(String result, String expectedResult) {
    return result.contains(expectedResult);
  }
}

package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.koans.VerificationType;

import java.util.List;

public class ChapterCreator5Update implements ChapterCreator {
  private static final String DB_KOANS_FIND = "db.koans.find();";

  private Koan createKoan1() {
    return new Koan(
        1,
        "Update one koan",
        """
            Update first koan name. [About updateOne](https://www.mongodb.com/docs/manual/reference/method/db.collection.updateOne/)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'first updated', order: 1},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', order: 2}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2}]);",
        DB_KOANS_FIND,
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'first updated', order: 1},{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', order: 2}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.updateOne({name: 'first'}, {$set: {name: 'first updated'});"
    );
  }

  private Koan createKoan2() {
    return new Koan(
        2,
        "Update one koan with new property",
        """
            Update first koan name and add source property Denkoroku.
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'first updated', order: 1, source: 'Denkoroku'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', order: 2}
            ]
            ```
                    """,
        "use mongo_koans; db.koans.insertMany([{name: 'first', order: 1}, {name: 'second', order: 2}]);",
        DB_KOANS_FIND,
        "[{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'first updated', order: 1, source: 'Denkoroku'},{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second', order: 2}]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.updateOne({name: 'first'}, {$set: {name: 'first updated', source: 'Denkoroku'});"
    );
  }

  private Koan createKoan3() {

    return new Koan(
        3,
        "Update all koan",
        """
            Update all Denkoroku koans with new property createdYear : 1268
            #### Command before your answer :\s
            ```
            use mongo_koans;
            db.koans.insertMany([
              {name: 'first', order: 1},
              {name: 'second', order: 2, source: 'Denkoroku'},
              {name: 'third', order: 3, source: 'Wu men guan'},
              {name: 'fourth', order: 4, source: 'Wu men guan'},
              {name: 'fifth', order: 5, source: 'Denkoroku'}
            ]);
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'first updated', order: 1},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second', order: 2, source: 'Denkoroku', createdYear: 1268)},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'third', order: 3, source: 'Wu men guan'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'fourth', order: 4, source: 'Wu men guan'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'fifth', order: 5, source: 'Denkoroku', createdYear: 1268)}
            ]
            ```
                    """,
        """
            use mongo_koans; db.koans.insertMany([
            {name: 'first', order: 1},
            {name: 'second', order: 2, source: 'Denkoroku'},
            {name: 'third', order: 3, source: 'Wu men guan'},
            {name: 'fourth', order: 4, source: 'Wu men guan'},
            {name: 'fifth', order: 5, source: 'Denkoroku'}
            ]);
            """,
        DB_KOANS_FIND,
        """
            [
            {_id: ObjectId("64b32033eb5c5e320771e624"), name: 'first', order: 1},
            {_id: ObjectId("64b32033eb5c5e320771e625"), name: 'second', order: 2, source: 'Denkoroku', createdYear: 1268},
            {_id: ObjectId("64b32033eb5c5e320771e626"), name: 'third', order: 3, source: 'Wu men guan'},
            {_id: ObjectId("64b32033eb5c5e320771e627"), name: 'fourth', order: 4, source: 'Wu men guan'},
            {_id: ObjectId("64b32033eb5c5e320771e628"), name: 'fifth', order: 5, source: 'Denkoroku', createdYear: 1268}
            ]""",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.updateOne({source: 'Denkoroku'}, {$set: {createdYear: 1268});"
    );
  }


  @Override
  public Chapter create() {
    return new Chapter(5, "Update", List.of(
        createKoan1(),
        createKoan2(),
        createKoan3()
    ));
  }
}

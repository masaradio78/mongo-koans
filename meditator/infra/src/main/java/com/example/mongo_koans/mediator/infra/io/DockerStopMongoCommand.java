package com.example.mongo_koans.mediator.infra.io;

import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DockerStopMongoCommand {
  private final CommandExecutor commandExecutor;
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public DockerStopMongoCommand(CommandExecutor commandExecutor) {
    this.commandExecutor = commandExecutor;
  }

  public void execute(String containerName) {
    commandExecutor.execute("docker stop " + containerName);
    log.info("container {} stopped", containerName);
  }
}

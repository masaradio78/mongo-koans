package com.example.mongo_koans.mediator.infra.io;

public class DockerCommandErrorException extends RuntimeException {
  public DockerCommandErrorException(String message) {
    super(message);
  }
}

package com.example.mongo_koans.mediator.infra.configurations;

import com.example.mongo_koans.mediator.infra.io.CommandExecutorImpl;
import org.example.mongo_koans.mediator.use_case.GetAllKoans;
import org.example.mongo_koans.mediator.use_case.GetOneKoan;
import org.example.mongo_koans.mediator.use_case.ProposeAnswer;
import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.example.mongo_koans.mediator.use_case.commands.ExecuteMongoCommands;
import org.example.mongo_koans.mediator.use_case.repositories.ChapterRepository;
import org.example.mongo_koans.mediator.use_case.repositories.MemoryChapterRepository;
import org.example.mongo_koans.mediator.use_case.verification.VerificationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KoansConfiguration {
  Logger log = LoggerFactory.getLogger(this.getClass());

  @Bean
  public ChapterRepository chapterRepository() {
    log.info("initialize chapter repository");
    return new MemoryChapterRepository();
  }

  @Bean
  public GetAllKoans getAllKoans(ChapterRepository chapterRepository) {
    log.info("initialize get all koans use case");
    return new GetAllKoans(chapterRepository);
  }

  @Bean
  public GetOneKoan getOneKoan(ChapterRepository chapterRepository) {
    log.info("initialize get one koan use case");
    return new GetOneKoan(chapterRepository);
  }

  @Bean
  public CommandExecutor commandExecutor() {
    return new CommandExecutorImpl();
  }

  @Bean
  public ProposeAnswer proposeAnswer(ChapterRepository chapterRepository, VerificationFactory verificationFactory, ExecuteMongoCommands executeMongoCommands) {
    return new ProposeAnswer(chapterRepository, verificationFactory, executeMongoCommands);
  }
}

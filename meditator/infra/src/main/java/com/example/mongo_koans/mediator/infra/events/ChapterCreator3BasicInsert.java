package com.example.mongo_koans.mediator.infra.events;

import org.example.mongo_koans.mediator.model.koans.Chapter;
import org.example.mongo_koans.mediator.model.koans.Koan;
import org.example.mongo_koans.mediator.model.koans.VerificationType;

import java.util.List;

public class ChapterCreator3BasicInsert implements ChapterCreator {

  private static final String USE_MONGO_KOANS = "use mongo_koans;";
  private static final String DB_KOANS_FIND = "db.koans.find();";

  private Koan createKoan1SingleDocument() {
    return new Koan(
        1,
        "Insert one document",
        """
            Insert one koan with name 'first' and order 1. [About insertOne](https://www.mongodb.com/docs/manual/reference/method/db.collection.insertOne/)
            #### Command before your answer :\s
            ```
            use mongo_koans;
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'first', order: 1}
            ]
                       
            ```
                    """,
        USE_MONGO_KOANS,
        DB_KOANS_FIND,
        "[" +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'first', order: 1}" +
            "]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.insertOne({name: 'first', order: 1});"
    );
  }

  private Koan createKoan2MultipleDocuments() {
    return new Koan(
        2,
        "Insert many documents",
        """
            Insert 2 koans, with name second and third by using insertMany
            #### Command before your answer :\s
            ```
            use mongo_koans;
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'third'}
            ]
                       
            ```
                    """,
        USE_MONGO_KOANS,
        DB_KOANS_FIND,
        "[" +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second'}," +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'third'}" +
            "]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.insertMany([{name: 'second'}, {name: 'third'}]);"
    );
  }

  private Koan createKoan3WithId() {
    return new Koan(
        3,
        "Insert one document with id",
        """
            Insert one koan with _id first and name second
            #### Command before your answer :\s
            ```
            use mongo_koans;
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: 'first', name: 'second'}
            ]
                       
            ```
                    """,
        USE_MONGO_KOANS,
        DB_KOANS_FIND,
        "[" +
            "{_id: 'first', name: 'second'}" +
            "]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.insertOne({_id: 'first', name: 'second'});"
    );
  }

  private Koan createKoan2MultipleDifferentDocuments() {
    return new Koan(
        4,
        "Insert many different documents",
        """
            Insert 2 koans, one with name second and second one with order 2
            #### Command before your answer :\s
            ```
            use mongo_koans;
            ```
            #### Command after your answer : `db.koans.find();` \s
            #### Expected result :\s
            ```
            [
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), name: 'second'},
              {_id: ObjectId("xxxxxxxxxxxxxxxx"), order: 2}
            ]
                       
            ```
                    """,
        USE_MONGO_KOANS,
        DB_KOANS_FIND,
        "[" +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), name: 'second'}," +
            "{_id: ObjectId(\"64b32033eb5c5e320771e624\"), order: 2}" +
            "]",
        VerificationType.WITHOUT_OBJECT_ID,
        "db.koans.insertMany([{name: 'second'}, {order: 2}]);"
    );
  }

  @Override
  public Chapter create() {
    return new Chapter(3, "Insert", List.of(
        createKoan1SingleDocument(),
        createKoan2MultipleDocuments(),
        createKoan3WithId(),
        createKoan2MultipleDifferentDocuments()
    ));
  }
}

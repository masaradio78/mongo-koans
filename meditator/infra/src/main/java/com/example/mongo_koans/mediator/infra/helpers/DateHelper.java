package com.example.mongo_koans.mediator.infra.helpers;

import java.time.LocalDateTime;

public interface DateHelper {
  LocalDateTime now();
}

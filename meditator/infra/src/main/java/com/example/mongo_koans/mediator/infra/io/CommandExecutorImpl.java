package com.example.mongo_koans.mediator.infra.io;

import org.example.mongo_koans.mediator.model.result.CommandResult;
import org.example.mongo_koans.mediator.model.result.ErrorCommand;
import org.example.mongo_koans.mediator.model.result.SuccessCommand;
import org.example.mongo_koans.mediator.use_case.commands.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Arrays;

@Component
public class CommandExecutorImpl implements CommandExecutor {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Override
  public CommandResult execute(java.lang.String command) {
    var commandSplitted = CommandSplitter.split(command);
    log.info(Arrays.toString(commandSplitted));
    ProcessBuilder processBuilder = new ProcessBuilder(commandSplitted);
    try {
      Process process = processBuilder.start();
      int exitCode = process.waitFor();
      log.info("Process exited with code: {}", exitCode);
      if (exitCode != 0) {
        return buildError(process, exitCode);
      }
      return buildSuccessResult(process);
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
      Thread.currentThread().interrupt();
      throw new ProcessRuntimeException(e);
    }
  }

  private ErrorCommand buildError(Process process, int exitCode) throws IOException {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    process.getErrorStream().transferTo(stream);
    log.warn("{}", stream);
    return new ErrorCommand(stream.toString(), exitCode);
  }

  private static SuccessCommand buildSuccessResult(Process process) throws IOException {
    StringBuilder outputBuilder = new StringBuilder();
    String line;
    InputStream inputStream = process.getInputStream();
    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
    while ((line = reader.readLine()) != null) {
      outputBuilder.append(line).append("\n");
    }
    return new SuccessCommand(outputBuilder.toString());
  }
}

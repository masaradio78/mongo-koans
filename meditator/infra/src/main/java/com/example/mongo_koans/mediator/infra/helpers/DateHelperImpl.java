package com.example.mongo_koans.mediator.infra.helpers;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DateHelperImpl implements DateHelper {
  @Override
  public LocalDateTime now() {
    return LocalDateTime.now();
  }
}
